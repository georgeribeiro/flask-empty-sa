#!/usr/bin/env python

import os
import sys

here = os.path.dirname(os.path.abspath(__file__))
sys.path.append(here)

def _make_context():
    from models import DBSession
    return dict(
        DBSession=DBSession,
        )

def main():
    from flask.ext.script import Manager, Shell
    from main import app_factory
    
    manager = Manager(app_factory)
    manager.add_option('-c', '--config', dest='config', required=False)
    manager.add_command('shell', Shell(make_context=_make_context))
    manager.run()

if __name__ == '__main__':
    main()
