from core.decorators import view

@view()
def index():
    project = 'Project Name'
    return {'project':project}
