def app_factory(config=None):
    from flask import Flask
    app = Flask(__name__)
    config_app(app, config)
    config_database(app)
    config_events(app)
    config_errorhandlers(app)
    config_context_processors(app)
    config_views(app)
    config_blueprints(app)
    return app

def config_app(app, config):
    """
    configure app
    """
    if config:
        app.config.from_pyfile(config)
    try:
        app.config.from_envvar('APP_CONFIG')
    except RuntimeError:
        pass

def config_database(app):
    """
    configure database
    """
    from models import initialize_sql
    from core.sa import engine_from_config
    engine = engine_from_config(app)
    if engine:
        initialize_sql(engine)

def config_events(app):
    """
    configure events
    """
    from models import DBSession
    @app.teardown_request
    def shutdown_session(exception=None):
        DBSession.remove()

def config_errorhandlers(app):
    """
    configure errors handlers
    """
    from flask import render_template
    @app.errorhandler(404)
    def page_not_found(error):
        return render_template('404.html'), 404

def config_context_processors(app):
    """
    configure context processors
    """

def config_views(app):
    """
    configure simple views
    """
    import views
    app.add_url_rule('/', view_func=views.index)

def config_blueprints(app):
    """
    configure blueprints
    """
    from core.utils import register_blueprints
    register_blueprints(app,
                        # ('views.auth.auth', '/auth'),
                        )
