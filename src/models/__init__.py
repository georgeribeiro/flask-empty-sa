from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session, mapper

DBSession = scoped_session(sessionmaker())
Base = declarative_base()
Base.query = DBSession.query_property()

def initialize_sql(engine):
    DBSession.configure(bind=engine)
    # import models here

    Base.metadata.bind = engine
    Base.metadata.create_all(engine)
