import sys

def import_object(name):
    """
    import object name
    """
    objectname = name.split('.')[-1]
    modulename = '.'.join(name.split('.')[:-1])
    __import__(modulename)
    module = sys.modules[modulename]
    return getattr(module, objectname)


def register_blueprints(app, *blueprints):
    """
    register blueprints
    """
    for blueprintname, url_prefix in blueprints:
        blueprint = import_object(blueprintname)
        app.register_blueprint(blueprint, url_prefix=url_prefix)
