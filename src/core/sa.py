
def engine_from_config(app, prefix='SQLALCHEMY_'):
    """
    create engine from configuration
    """
    uri = app.config.get(prefix + 'DATABASE_URI')
    echo = app.config.get(prefix + 'ECHO')
    engine = None
    if uri:
        engine = create_engine(uri, echo=echo)
    return engine
