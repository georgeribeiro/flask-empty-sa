from flask import render_template, Response, request
from werkzeug.wrappers import BaseResponse
from functools import wraps
from serialize import CustomJSONEncoder
import json as _json

def view(renderer=None):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            r = f(*args, **kwargs)
            if isinstance(r, BaseResponse):
                return r
            if renderer is None or renderer.endswith('.html'):
                template = renderer or request.endpoint.replace('.', '/') + '.html'
                context = r or {}
                return render_template(template, **context)
            elif renderer == 'json':
                json = _json.dumps(r, cls=CustomJSONEncoder, indent=4)
                return Response(json, mimetype='text/javascript')
        return decorated_function
    return decorator
