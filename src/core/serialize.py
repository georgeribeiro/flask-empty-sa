from json import JSONEncoder
from decimal import Decimal
from datetime import datetime, date

class CustomJSONEncoder(JSONEncoder):
    format_dt = '%Y-%m-%d %H:%M:%S'
    format_da = '%Y-%m-%d'
    iterators = ('iterator', 'listiterator', 'generator',)
    
    def default(self, o):
        if hasattr(o, '__json__'): # optional get json
            return o.__json__()
        
        if isinstance(o, Decimal): # decimal
            return str(o)

        elif isinstance(o, datetime): # datetime
            return o.strftime(self.format_dt)
        
        elif isinstance(o, date): # date
            return o.strftime(self.format_da)
        
        elif o.__class__.__name__ in self.iterators:
            return [_ for _ in o]
        
        if hasattr(o, '__mapper__'): # instance from Base (sqlalchemy)
            r = {}
            for a in dir(o):
                if not a.startswith('_') and a != 'metadata':
                    r[a] = getattr(o, a)
            return r
        
        try:
            return vars(o) # any object
        except TypeError:
            return super(CustomJSONEncoder, self).default(o)
